const AWS = require('aws-sdk');
var s3 = new AWS.S3();

const UserPoolId = process.env.USER_POOL_ID;
const BucketName = process.env.S3_BUCKET_NAME;

var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

module.exports.s3FileUpload = (file) => {

    let params = {
        Bucket: BucketName,
        Key: file.profile_img.filename,
        Body: file.profile_img.content,
        ContentType: 'image/jpeg'
    }

    return new Promise(function(resolve, reject) {
        s3.putObject(params, (err, data) => {
            if(err){
                reject(err);
            }
            resolve(data);
        });
    });
};

module.exports.getCognitoUser = (data) => {
    var params = {
        UserPoolId: UserPoolId,
        Username: 'bhavar1011@yopmail.com',
    };

    return new Promise(function(resolve, reject) {
        cognitoidentityserviceprovider.adminGetUser(params, function(err, data) {
            if (err){
                reject(err);
            }
            resolve(data);
        });
    });
}

module.exports.addCognitoUser = (data) => {
    let params = {
        UserPoolId: UserPoolId,
        Username: 'bhavar1011@yopmail.com',
        TemporaryPassword: 'helloWorld1@',
        UserAttributes: [ 
            {
              Name: "nickname", 
              Value: "Bhanwar"
            },
        ]
    };

    return new Promise(function(resolve, reject) {
        cognitoidentityserviceprovider.adminCreateUser(params, function(err, data) {
            if (err){
                reject(err);
            }
            resolve(data);
        });
    });
}

module.exports.updateCognitoUser = (data) => {
    var params = {
        UserPoolId: UserPoolId, 
        Username: 'bhavar1011@yopmail.com',
        UserAttributes: [ 
            {
                Name: "nickname", 
                Value: "Bhavesh"
            },
        ]
    };
    return new Promise(function(resolve, reject) {
        cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function(err, data) {
            if (err){
                reject(err);
            }
            resolve(data);
        });
    });
}

module.exports.deleteCognitoUser = (data) => {
    var params = {
        UserPoolId: UserPoolId, 
        Username: 'bhavar1011@yopmail.com' 
    };

    return new Promise(function(resolve, reject) {
        cognitoidentityserviceprovider.adminDeleteUser(params, function(err, data) {
            if (err){
                reject(err);
            }
            resolve(data);
        });
    });
}