
module.exports.responseBuilder = (statusCode, success, data, message="success") => {

    let header = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    }

    let body = JSON.stringify({
        success: success,
        data: data,
        message: message
    }) 
    return {
        statusCode: statusCode,
        headers: header,
        body: body
    }
};