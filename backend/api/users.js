const user = require('../models/user.js');
const utils = require('../helper/utils.js');
const awsService = require('../helper/aws-service.js');

const multipart = require('aws-lambda-multipart-parser')

module.exports.index = (event, context, callback) => {
    callback(null, utils.responseBuilder(200, true, [], "welcome to user api V1"));
};

module.exports.listUsers = (event, context, callback) => {
    user.getAll().then((data)=>{
        callback(null, utils.responseBuilder(200, true, data));
    }).catch((err)=>{
        console.log(err);
        callback(null, utils.responseBuilder(500, false, [], "Something went wrong!"));
    });
};

module.exports.getUsersCount = (event, context, callback) => {
    user.getCount().then((data)=>{
        callback(null, utils.responseBuilder(200, true, data));
    }).catch((err)=>{
        console.log(err);
        callback(null, utils.responseBuilder(500, false, [], "Something went wrong!"));
    });
};

module.exports.getUserById = (event, context, callback) => {
    user.findById(event.pathParameters.id).then((data)=>{
        if(data.length > 0){
            callback(null, utils.responseBuilder(200, true, data));
        }else{
            callback(null, utils.responseBuilder(200, false, [], "No record found"));
        }
    }).catch((err)=>{
        console.log(err);
        callback(null, utils.responseBuilder(500, false, [], "Something went wrong!"));
    });
};

module.exports.uploadUserProfileImg = (event, context, callback) => {
    
    let file = multipart.parse(event, true);

    let updateData = {
        profile_img: file.profile_img.filename
    }

    awsService.s3FileUpload(file).then(() => {
        user.update(event.pathParameters.id, updateData).then((data)=>{
            callback(null, utils.responseBuilder(200, true, data, "File uploaded successfully"));
        }).catch((err)=>{
            console.log(err);
            callback(null, utils.responseBuilder(500, false, [], "Something went wrong!"));
        });
    }).catch((err) => {
        console.log(err);
        callback(null, utils.responseBuilder(500, false, [], "Something went wrong!"));
    });
};

module.exports.getCognitoUser = (event, context, callback) => {
    awsService.getCognitoUser().then((data) => {
        callback(null, utils.responseBuilder(200, true, data));
    }).catch((err) => {
        console.log(err);
        callback(null, utils.responseBuilder(500, false, [], "Something went wrong!"));
    })
};

module.exports.addCognitoUser = (event, context, callback) => {
    awsService.addCognitoUser().then((data) => {
        callback(null, utils.responseBuilder(200, true, data, "User created in user pool"));
    }).catch((err) => {
        console.log(err);
        callback(null, utils.responseBuilder(500, false, [], "Something went wrong!"));
    })
};

module.exports.updateCognitoUser = (event, context, callback) => {
    awsService.updateCognitoUser().then((data) => {
        callback(null, utils.responseBuilder(200, true, data, "User updated in user pool"));
    }).catch((err) => {
        console.log(err);
        callback(null, utils.responseBuilder(500, false, [], "Something went wrong!"));
    })
};

module.exports.deleteCognitoUser = (event, context, callback) => {
    awsService.deleteCognitoUser().then((data) => {
        callback(null, utils.responseBuilder(200, true, data, "User delete from user pool"));
    }).catch((err) => {
        console.log(err);
        callback(null, utils.responseBuilder(500, false, [], "Something went wrong!"));
    })
};

