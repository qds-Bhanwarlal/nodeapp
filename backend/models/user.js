const knex = require('../knex/knex.js');

const table_name = 'users';

module.exports.getAll = () => {
    return new Promise(function(resolve, reject) {
        knex.select()
            .table(table_name)
            .then(users => {
                resolve(users);
            }).catch(err => {
                reject(err);
            });
    });
};

module.exports.getCount = () => {
    return new Promise(function(resolve, reject) {
        knex(table_name)
            .count('*', {as: 'user_count'})
            .then(count => {
                resolve(count);
            }).catch(err => {
                reject(err);
            });
    });
};

module.exports.findById = (id) => {
    return new Promise(function(resolve, reject) {
        knex(table_name)
            .where('id', '=', id)
            .then(user => {
                resolve(user);
            }).catch(err => {
                reject(err);
            });
    });
};

module.exports.update = (id, data) => {
    return new Promise(function(resolve, reject) {
        knex(table_name)
            .where('id', '=', id)
            .update(data)
            .then(user => {
                resolve(user);
            }).catch(err => {
                reject(err);
            });
    });
};
