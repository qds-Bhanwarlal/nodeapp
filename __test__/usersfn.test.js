
var awsService = require('../backend/helper/aws-service.js');

const AWS = require('aws-sdk')
const AWSMock = require('aws-sdk-mock')

describe('landing', () => {
    it("should mock getItem from DynamoDB", async () => {
        AWSMock.setSDKInstance(AWS);
        AWSMock.mock('DynamoDB', 'getItem', (getItemInput, callback) => {
          callback(null, {pk: "foo", sk: "bar"});
        })
     
        let getItemInput = { TableName: '', Key: {} };
        const dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'});
        expect(await dynamodb.getItem(getItemInput).promise()).toStrictEqual( { pk: 'foo', sk: 'bar' });
     
        AWSMock.restore('DynamoDB');
    });

    it("should mock", done => {
        AWSMock.setSDKInstance(AWS);
        AWSMock.mock('CognitoIdentityServiceProvider', 'adminGetUser', (params, callback) => {
          callback(null, {pk: "foo", sk: "bar"});
        })

        var params = {
            UserPoolId: 'XYZ',
            Username: 'ABC',
        };
        
        const cognito = new AWS.CognitoIdentityServiceProvider();
        expect(await cognito.adminGetUser(params).promise()).toStrictEqual( { pk: 'foo', sk: 'bar' });
     
        AWSMock.restore('CognitoIdentityServiceProvider');
    });
});
