const request = require('supertest')
var FormData = require('form-data');
var fs = require('fs');

const path = "http://localhost:3000";

describe('landing', () => {
    it('Should return the ', (done) => {
        request(path)
            .get('/')
            .expect(200)
            .then(res => {
                done();
            });
    })
});


describe('Users', () => {
    it('Should return all the users', (done) => {
        request(path)
            .get('/users')
            .expect(200)
            .then(res => {
                expect(res.body.success).toBe(true);
                expect.arrayContaining(res.body.data);
                done();
            });
    })


    describe('Find User by Id', () => {
        it('Should return user', (done) => {
            request(path)
                .get('/users/1')
                .expect(200)
                .then(res => {
                    expect(res.body.success).toBe(true);
                    expect(res.body.data.length).toBe(1);
                    expect(res.body.data[0].id).toBe(1);
                    done();
                });
        })

        it('Should return message user not found', (done) => {
            request(path)
                .get('/users/9')
                .expect(200)
                .then(res => {
                    expect(res.body.success).toBe(false);
                    expect.stringContaining(res.body.message);
                    expect(res.body.message).toBe("No record found");
                    done();
                });
        })

        it('Should return error message', (done) => {
            request(path)
                .get('/users/9test')
                .expect(500)
                .then(res => {
                    expect(res.body.success).toBe(false);
                    expect.stringContaining(res.body.message);
                    expect(res.body.message).toBe("Something went wrong!");
                    done();
                });
        })
    })

    it('Should return users count', (done) => {
        request(path)
            .get('/users/count')
            .expect(200)
            .then(res => {
                expect(res.body.success).toBe(true);
                expect.arrayContaining(res.body.data);
                expect(res.body.data.user_count);
                done();
            });
    })

    it('Should return file upload error', (done) => {
        var form = new FormData();
        const file = fs.readFileSync(__dirname+'/profile_image.jpg');
        form.append('profile_img', file);

        form.submit(path+'/users/1/upload', (err, res)=>{
            expect(res.statusCode).toBe(500);
            done();
        })
    })
})


describe('Cognito', () => {
    it('Should add user to cognito user pool', (done) => {
        request(path)
            .post('/users/cognito')
            .expect(500)
            .then(res => {
                done();
            });
    });

    it('Should get the user from cognito user pool', (done) => {
        request(path)
            .post('/users/cognito')
            .expect(500)
            .then(res => {
                done();
            });
    });
    
    it('Should update user of cognito user pool', (done) => {
        request(path)
            .post('/users/cognito/update')
            .expect(500)
            .then(res => {
                done();
            });
    });

    it('Should delete the user of cognito user pool', (done) => {
        request(path)
            .delete('/users/cognito')
            .expect(500)
            .then(res => {
                done();
            });
    })
});