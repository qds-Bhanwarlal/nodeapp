### NodeJs + PostgreSQL + Jest + Serverless Framework + CI/CD

#### Setup Commands

``` 
$ npm i
$ npm i -g serverless 
$ npm i -g knex 
```

#### DB Setup

Update DB config in knex/knexfile.js
**Run Migration**: ```$ knex migrate:up ```
**Run Seed**: ```$ knex seed:run ```

#### Commands
``` 
$ npm start                 // start project
$ npm test                  // run test cases
$ npm run test:coverage     // run test cases and generate code coverage report
```


#### Routes
| Method | Path | Description |
| ------ | ------ | ------ |
| GET  | /users | Get all users |
| GET  | /users/count | Get users count |
| GET  | /users/{id} | Get user by Id |
| GET  | /users/cognito | Get users from Cognito User Pool |
| POST  | /users/cognito | Add users to Cognito User Pool |
| POST  | /users/cognito/update | Update users of Cognito User Pool |
| DELETE  | /users/cognito | Delete users from Cognito User Pool |